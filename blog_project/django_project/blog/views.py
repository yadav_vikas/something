from django.shortcuts import render
from django.http import HttpResponse


posts = [
    {
        'author': 'CoreyMS',
        'title': 'Blog post 1',
        'content': 'first post content',
        'date_posted':'august 27,2020'
    },
    {
        'author': 'Vikas yadav',
        'title': 'Blog post 2',
        'content': '2nd post content',
        'date_posted':'DEC 24,2020'
    }

]

# Create your views here.
def home(request):
    context = {
        'posts':posts
    }
    """[summary]

    Args:
        request ([type]): [description]

    Returns:
        [type]: [description]
    """
    return render(request,'blog/home.html',context)

def about(request):
    return render(request,'blog/about.html',{'title':'About'})