from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def hello(request):
    return HttpResponse("Hello")

def home(request):
    return render(request,'form/base.html')

def sample(request):
    return render(request,'form/home.html')
def printer(request):
    return render(request,'form/printer.html')
    