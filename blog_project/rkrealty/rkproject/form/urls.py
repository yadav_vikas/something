
from django.urls import path
from . import views

urlpatterns = [
    path('hello/',views.hello,name='hello-world'),
    path('home/',views.home,name='home'),
    path('sample/',views.sample,name='sample'),
    path('',views.printer,name='printer'),
]
